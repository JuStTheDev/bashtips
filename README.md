### Content

Presentation about some useful bash-hacks and terminal usage in general.

### Setup

1. Install dependencies
   ```sh
   $ yarn install
   ```

1. Serve the presentation and monitor source files for changes
   ```sh
   $ yarn start
   ```

1. Open <http://localhost:8000> to view the presentation

   You can change the port by using `npm start -- --port=8001`.

